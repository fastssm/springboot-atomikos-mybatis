package com.pro.jta.mapper.databases1;

import org.springframework.stereotype.Repository;

import com.pro.jta.domain.User;

@Repository
public interface UserMapper {
	Integer add (User user) throws Exception;
	Integer del () throws Exception;
}
