package com.pro.jta.mapper.databases2;

import org.springframework.stereotype.Repository;

import com.pro.jta.domain.Test;

@Repository
public interface TestMapper {
	Integer add (Test test) throws Exception;
	Integer del () throws Exception;
}
