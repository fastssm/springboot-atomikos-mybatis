package com.pro.jta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.atomikos.util.ClassLoadingHelper;
import com.pro.jta.base.datasource.Databases1Config;
import com.pro.jta.base.datasource.Databases2Config;



@SpringBootApplication
@EnableConfigurationProperties(value = { Databases1Config.class,Databases2Config.class })
public class Application {

	public static void main(String[] args) {
		System.setProperty("com.atomikos.icatch.file", ClassLoadingHelper.loadResourceFromClasspath(Application.class, "transactions.properties").getPath());
		SpringApplication.run(Application.class, args);
	}
}
