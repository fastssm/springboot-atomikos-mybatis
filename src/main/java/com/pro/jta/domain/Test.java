package com.pro.jta.domain;

import java.io.Serializable;

public class Test implements Serializable{
	private static final long serialVersionUID = -3887250167772644590L;
	
	private Long id;
	private String test;
	
	public Test(){};
	
	public Test(String test){
		this.test = test;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTest() {
		return test;
	}
	public void setTest(String test) {
		this.test = test;
	}
}
