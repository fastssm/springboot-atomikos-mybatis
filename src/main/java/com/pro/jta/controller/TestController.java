package com.pro.jta.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pro.jta.service.UserService;


@RestController
public class TestController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/")
	public String index(){
		userService.save();
		return "lalalla";
	}
	
	@RequestMapping("/del")
	public String del(){
		userService.del();
		return "del";
	}
}
