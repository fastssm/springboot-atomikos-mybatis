package com.pro.jta.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pro.jta.ao.UserAo;
import com.pro.jta.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserAo userAo;

	@Override
	public Integer save() {
		//这里必须要抛出异常，不然JTA不会回滚数据
		try {
			userAo.save();
			userAo.del();
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return 1;
	}

	@Override
	public void del() {
		try {
			userAo.del();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}