package com.pro.jta.base.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.atomikos.jdbc.AtomikosDataSourceBean;
import com.mysql.jdbc.jdbc2.optional.MysqlXADataSource;
import com.pro.jta.base.datasource.Databases1Config;


@Configuration
// basePackages 最好分开配置 如果放在同一个文件夹可能会报错
@MapperScan(basePackages = "com.pro.jta.mapper.databases1", sqlSessionTemplateRef = "databases1SqlSessionTemplate")
public class Databases1MyBatisConfig {

	// 配置数据源
	@Primary
	@Bean(name = "databases1DataSource")
	public DataSource databases1DataSource(Databases1Config databases1Config) throws SQLException {
		MysqlXADataSource mysqlXaDataSource = new MysqlXADataSource();
		mysqlXaDataSource.setUrl(databases1Config.getUrl());
		mysqlXaDataSource.setPinGlobalTxToPhysicalConnection(true);
		mysqlXaDataSource.setPassword(databases1Config.getPassword());
		mysqlXaDataSource.setUser(databases1Config.getUsername());
		mysqlXaDataSource.setPinGlobalTxToPhysicalConnection(true);
		
		AtomikosDataSourceBean xaDataSource = new AtomikosDataSourceBean();
		xaDataSource.setXaDataSource(mysqlXaDataSource);
		xaDataSource.setUniqueResourceName("databases1DataSource");

		xaDataSource.setMinPoolSize(databases1Config.getMinPoolSize());
		xaDataSource.setMaxPoolSize(databases1Config.getMaxPoolSize());
		xaDataSource.setMaxLifetime(databases1Config.getMaxLifetime());
		xaDataSource.setBorrowConnectionTimeout(databases1Config.getBorrowConnectionTimeout());
		xaDataSource.setLoginTimeout(databases1Config.getLoginTimeout());
		xaDataSource.setMaintenanceInterval(databases1Config.getMaintenanceInterval());
		xaDataSource.setMaxIdleTime(databases1Config.getMaxIdleTime());
		return xaDataSource;
	}

	@Bean(name = "databases1SqlSessionFactory")
	public SqlSessionFactory databases1SqlSessionFactory(@Qualifier("databases1DataSource") DataSource dataSource)
			throws Exception {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(dataSource);
		bean.setMapperLocations(
				new PathMatchingResourcePatternResolver().getResources("classpath:/mybatis/databases1/*.xml"));
		return bean.getObject();
	}

	@Bean(name = "databases1SqlSessionTemplate")
	public SqlSessionTemplate databases1SqlSessionTemplate(
			@Qualifier("databases1SqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}
}
