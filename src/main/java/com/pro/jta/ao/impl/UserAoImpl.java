package com.pro.jta.ao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pro.jta.ao.UserAo;
import com.pro.jta.domain.User;
import com.pro.jta.domain.Test;
import com.pro.jta.mapper.databases1.UserMapper;
import com.pro.jta.mapper.databases2.TestMapper;

@Service
public class UserAoImpl implements UserAo{
	
	private static Logger logger = Logger.getLogger(UserAoImpl.class);
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private TestMapper testMapper;

	@Override
	public Integer save() throws Exception {
		userMapper.add(new User("pro", 11));
		testMapper.add(new Test("test"));
		logger.info("save ok");
		return 1;
	}

	@Override
	public void del() throws Exception{
		userMapper.del();
		System.out.println(1/0);
		testMapper.del();
	}
}
